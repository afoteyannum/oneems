#!/bin/ksh

LOGDIR=/usr/apps/oneems/log

print "#ofRanges #/Range UniqueIP"
grep "running SNMP for ipaddr " $LOGDIR/discovery.log | cut -d'.' -f2,3 | uniq -c | cat -n
print
print "Current breakdown"
grep "Obtained flag for concatenated frame" $LOGDIR/discovery.log | cut -d'{' -f2 | sort | uniq -c

print
TOTAL=$(grep -c "ip_range_calculate.py ipaddr" $LOGDIR/discovery.log)
RUNNING_TOTAL=$(grep -c "running SNMP for ipaddr " $LOGDIR/discovery.log)
(( PERCENTAGE_DONE = $RUNNING_TOTAL * 100/$TOTAL ))
print "Total IPs being discovered = $TOTAL"
print "Total IPs completed = $RUNNING_TOTAL : $PERCENTAGE_DONE%"
print -n "Started "
head -1 /usr/apps/oneems/log/discovery.log | cut -d'-' -f2-4
print -n "Last Updated "
tail -1 /usr/apps/oneems/log/discovery.log | cut -d'-' -f2-4
