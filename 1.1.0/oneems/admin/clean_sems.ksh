#!/bin/ksh

ipcs -s | grep apache > apache_sems.out
ipcs -m | grep apache > apache_shmems.out

while read line
do
        set $line
        print "ipcrm -s $2"
done < apache_sems.out

while read line
do
        set $line
        print "ipcrm -m $2"
done < apache_shmems.out

