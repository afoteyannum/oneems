import re
import json
import datetime
import connexion
import subprocess
from log_ import logger
from ip_detect import detectTypeIP
from extractor import extractor_object
from dynamic_mapper import 	insert_into_table, \
							schema_map, \
							fetch_from_db, \
							update_row_table, \
							df_kw_from_db, \
							mix_frames, \
							compare_kw_frame, \
							check_frame, \
							get_table_schema, \
							delete_frame, \
							frame_dvalue_populate, \
							DEBUG


AS_API = True

mapper = {"1.3.6.1.2.1.1":extractor_object.run_all}

def get_result(command):
	proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
	return proc.stdout.read()

def snmp_walk(device_type, ip_addr, command_):
	response = {"result": None,"sucess":False}
	if True:
		command = "snmpwalk -v2c -c %s %s %s"%(str(device_type),str(ip_addr),str(command_))
		result = get_result(command)
		sw_func = mapper.get(command_,None)
		response = {"status":False,"data":{}}
		if sw_func:
			response = sw_func(result)
		return response
	else:
		response["result"] = "[x] Unable to PING"
	return response

def get_snmp_frame(ipaddr):
	result = {"status":False,"snmp_frame":None}
	snmp_frame =  snmp_walk("2Y2LHTZP31", xaddr, "1.3.6.1.2.1.1")
	if snmp_frame["status"]:
		if snmp_frame["data"]:
			snmp_frame = snmp_frame["data"]
			if DEBUG:
				log_message = "snmp_run.py SNMP frame obtained : {frame}".format(frame=str(snmp_frame))
				logger.info(log_message)
			result["status"] = True
		else:
			snmp_frame = None
		result["snmp_frame"] = snmp_frame
	return result



def flag_kw(**kwargs):
	node_frame = kwargs.get("node_frame",None)
	snmp_frame = kwargs.get("snmp_frame",None)
	class_ = "I"

	snmp_frame_status = check_frame(frame=snmp_frame,ignore = ["deviceos","model","lastpolled","market","discoverystatus"])
	snmp_available = False
	if snmp_frame_status["status"]:
		if snmp_frame_status["data"]:
			snmp_available = True

	node_frame_status = check_frame(frame=node_frame, ignore=["market"])
	node_available = False
	if node_frame_status["status"]:
		if node_frame_status["data"]:
			node_available = True

	if snmp_available:
		if node_available:
			equal_ = compare_kw_frame(snmp_frame=snmp_frame,node_frame=node_frame,labels=["nodeVersion","devicename"])
			if equal_["status"]:
				if equal_["data"]:
					class_ = "K"
				else:
					class_ = "C"
		else:
			class_ = "N"
	else:
		if node_available:
			class_ = "M"
	return {"status":True,"data":class_}



def snmp_api(ip_addr):
	frames_collections = []
	global schema_map
	ipaddr = str(ip_addr)
	response = {"deviceIpAddr":False,"data":False}
	if DEBUG:
		log_message =  "running SNMP for ipaddr : {ipaddr}".format(ipaddr=str(ipaddr))
		logger.info(log_message)
	type_ipaddr = detectTypeIP(ipaddr)
	if type_ipaddr == 1:
		ipaddr = "udp6:[{ipaddr}]:161".format(ipaddr=ipaddr)
	snmp_frame =  snmp_walk("2Y2LHTZP31", ipaddr, "1.3.6.1.2.1.1")
	node_frame =  fetch_from_db(	tblname="nodes", filter_={"deviceIpAddr":str(ipaddr)})
	class_ = ""
	status_frame = {"snmp_frame":False,"node_frame":False}

	if snmp_frame["status"]:
		if snmp_frame["data"]:
			snmp_frame = snmp_frame["data"]
			if DEBUG:
				log_message = "SNMP frame obtained : {frame}".format(frame=str(snmp_frame))
				logger.info(log_message)
			status_frame["snmp_frame"] = True
		else:
			snmp_frame = None

	if node_frame["status"]:
		if node_frame["data"]:
			node_frame = node_frame["data"][0]
			if DEBUG:
				log_message = "NODES frame obtained : {frame}".format(frame=str(node_frame))
				logger.info(log_message)
			status_frame["node_frame"] = True
		else:
			node_frame = None

	concatenated_frame = snmp_frame
	flag_obtain = flag_kw(snmp_frame=snmp_frame,node_frame=node_frame)
			
	"""if (snmp_frame or node_frame): 
		cframe = mix_frames(frameA=snmp_frame or node_frame,frameB=node_frame,labels=["deviceIpAddr"],updates={"deviceIpAddr":str(ipaddr)})
		if cframe["status"]:
			concatenated_frame = cframe["data"] 
				 
		if DEBUG:
		log_message = "[i] CONCATENATED FRAME {frame}".format(frame=str(concatenated_frame))
		logger.info(log_message) """
	if (snmp_frame or node_frame): 		
		if flag_obtain["status"]:
			"""if DEBUG:
				log_message = "Obtained flag for concatenated frame {flag}".format(flag=str(flag_obtain))
				logger.info(log_message)"""
			cframe = mix_frames(frameA=concatenated_frame,frameB=None,updates={	"nodestatus":flag_obtain["data"]})
			if cframe["status"]:
				concatenated_frame.update(cframe["data"])
				#concatenated_frame.update(concatenated_frame["data"])
				#concatenated_frame.pop("data")
	
	response["data"] = concatenated_frame
	response["deviceIpAddr"] = ipaddr
	#response = { "deviceipaddress" : ipaddr :{ concatenated_frame } }				
	return response

  
if AS_API:
	walk_api = connexion.App(__name__)
	walk_api.add_api('snmp_walk.yaml')

	# application = app.app
	if __name__ == '__main__':
	    walk_api.run(port=8085)
else:
	snmp_api("10.198.238.5")

