#!/bin/ksh

ps -ef | grep snmp_walk.py | grep -qv grep
res=$?
if [ $res == "1" ]
then
	current_dt=`date`
	print "$current_dt: Restarted manual discovery" >> /usr/apps/oneems/log/man_dscv.log 2>&1
	/bin/python /usr/apps/oneems/python/manual_discovery/snmp_walk.py >> /usr/apps/oneems/log/man_dscv.log 2>&1
fi
