import re
import json
import ipcalc
import datetime
import ipaddress
import connexion
import subprocess
from log_ import logger
from deepdiff import DeepDiff
from extractor import extractor_object
from ipallocation import get_ipaddrs
from json_assembler import output_assembler
from dynamic_mapper import 	insert_into_table, \
							schema_map, \
							fetch_from_db, \
							update_row_table, \
							df_kw_from_db, \
							mix_frames, \
							compare_kw_frame, \
							check_frame, \
							get_table_schema, \
							delete_frame, \
							frame_dvalue_populate, \
							DEBUG


AS_API = False

mapper = {"1.3.6.1.2.1.1":extractor_object.run_all}

def get_result(command):
	proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
	return proc.stdout.read()

def snmp_walk(device_type, ip_addr, command_):
	response = {"result": None,"sucess":False}
	if True:
		command = "snmpwalk -v2c -c %s %s %s"%(str(device_type),str(ip_addr),str(command_))
		result = get_result(command)
		sw_func = mapper.get(command_,None)
		response = {"status":False,"data":{}}
		if sw_func:
			response = sw_func(result)
		return response
	else:
		response["result"] = "[x] Unable to PING"
	return response

def get_snmp_frame(xaddr):
	result = {"status":False,"snmp_frame":None}
	snmp_frame =  snmp_walk("2Y2LHTZP31", xaddr, "1.3.6.1.2.1.1")
	if snmp_frame["status"]:
		if snmp_frame["data"]:
			snmp_frame = snmp_frame["data"]
			if DEBUG:
				log_message = "snmp_run.py SNMP frame obtained : {frame}".format(frame=str(snmp_frame))
				logger.info(log_message)
			result["status"] = True
		else:
			snmp_frame = None
		result["snmp_frame"] = snmp_frame
	return result



def flag_kw(**kwargs):
	node_frame = kwargs.get("node_frame",None)
	snmp_frame = kwargs.get("snmp_frame",None)
	class_ = "I"

	snmp_frame_status = check_frame(frame=snmp_frame,ignore = ["deviceos","model","datepolled","timepolled","lastpolled","market","deviceIpAddr"])
	snmp_available = False
	if snmp_frame_status["status"]:
		if snmp_frame_status["data"]:
			snmp_available = True

	node_frame_status = check_frame(frame=node_frame, ignore=["market"])
	node_available = False
	if node_frame_status["status"]:
		if node_frame_status["data"]:
			node_available = True

	if snmp_available:
		if node_available:
			equal_ = compare_kw_frame(snmp_frame=snmp_frame,node_frame=node_frame,labels=["nodeVersion","devicename"])
			if equal_["status"]:
				if equal_["data"]:
					class_ = "K"
				else:
					class_ = "C"
		else:
			class_ = "N"
	else:
		if node_available:
			class_ = "M"
	return {"status":True,"data":class_}



def snmp_api(market):
	frames_collections = []
	global schema_map
	xaddrs = get_ipaddrs(market)
	if xaddrs["status"]:
		xaddrs = xaddrs["ipaddrs"]
		for xaddr in xaddrs:
			xaddr = str(xaddr)
			if DEBUG:
				log_message =  "running SNMP for ipaddr : {ipaddr}".format(ipaddr=str(xaddr))
				logger.info(log_message)
			snmp_frame =  snmp_walk("2Y2LHTZP31", xaddr, "1.3.6.1.2.1.1")
			print snmp_frame
			node_frame =  fetch_from_db(	tblname="nodes", filter_={"deviceIpAddr":str(xaddr)})
			class_ = ""
			status_frame = {"snmp_frame":False,"node_frame":False}

			if snmp_frame["status"]:
				if snmp_frame["data"]:
					snmp_frame = snmp_frame["data"]
					if DEBUG:
						log_message = "SNMP frame obtained : {frame}".format(frame=str(snmp_frame))
						logger.info(log_message)
					status_frame["snmp_frame"] = True
				else:
					snmp_frame = None

			if node_frame["status"]:
				if node_frame["data"]:
					node_frame = node_frame["data"][0]
					if DEBUG:
						log_message = "NODES frame obtained : {frame}".format(frame=str(node_frame))
						logger.info(log_message)
					status_frame["node_frame"] = True
				else:
					node_frame = None

			concatenated_frame = snmp_frame
			flag_obtain = flag_kw(snmp_frame=snmp_frame,node_frame=node_frame)
			
			if (snmp_frame or node_frame): 
				cframe = mix_frames(frameA=snmp_frame or node_frame,frameB=node_frame,labels=["submarket","market","csr_site_name","csr_site_id","region"],updates={"deviceIpAddr":str(xaddr)})
				if cframe["status"]:
					concatenated_frame = cframe["data"]
				 
			if DEBUG:
				log_message = "[i] CONCATENATED FRAME {frame}".format(frame=str(concatenated_frame))
				logger.info(log_message)
			
			if flag_obtain["status"]:
				if DEBUG:
					log_message = "Obtained flag for concatenated frame {flag}".format(flag=str(flag_obtain))
					logger.info(log_message)
				cframe = mix_frames(frameA=concatenated_frame,frameB=None,updates={	"class":flag_obtain["data"],
																					"market":str(market)})
				if cframe["status"]:
					concatenated_frame = cframe["data"]

			frames_collection = {	"schema_map":schema_map,
									"concatenated_frame":concatenated_frame,
									"flag": flag_obtain["data"],
									"xaddr": str(xaddr),
								}
			frames_collections = output_assembler(frames_collections,updates=frames_collection)
		delete_entries = fetch_from_db(tblname="discoveryres",filter_={"market":market})
		if delete_entries["status"]:
			delete_entries = delete_entries["data"]
			for delete_entry in delete_entries:
				id_ = delete_entry.get("id",None)
				if id_:
					delete_frame(tblname="discoveryres",conditions={"id":[id_]})

	for json_p in frames_collections:
		schema_map = json_p["schema_map"]	
		concatenated_frame = json_p["concatenated_frame"]	
		flag = json_p["flag"]
		xaddr = json_p["xaddr"]
		if flag == "I":
			if DEBUG:
				log_message = "[i] FRAME(ignored) : {frame}".format(frame=concatenated_frame)
				logger.info(log_message)
		else:
			tbl_to_insert = "discoveryres"
			conditions = {"deviceIpAddr":[str(xaddr)]}
			blank_schema = False
			insert_into_table(	schema=schema_map,
							dbname="oneems",
							tblname="discoveryres",
							updates=concatenated_frame,
							conditions=conditions,												
							ignores=["id"],
							blank_schema=blank_schema 	)
							
	for json_p in frames_collections:
		schema_map = json_p["schema_map"]	
		concatenated_frame = json_p["concatenated_frame"]	
		flag = json_p["flag"]
		xaddr = json_p["xaddr"]

		if flag == "I":
			if DEBUG:
				log_message = "[i] FRAME(ignored) : {frame}".format(frame=concatenated_frame)
				logger.info(log_message)
			

		elif flag == "N":
			tbl_to_insert = "discoveryres"
			#conditions = {"deviceIpAddr":[str(xaddr)]}
			blank_schema = False
			
			node_frame = get_table_schema(tblname="nodes",blank=True)
			if node_frame["status"]:
				node_frame = node_frame["data"]
				cframe = mix_frames(frameA=node_frame,frameB=concatenated_frame,labels=["submarket","market","deviceos","devicename","nodeVersion","csr_site_name","csr_site_id","region","model","lastpolled"],updates={"deviceIpAddr":str(xaddr)})
				if cframe["status"]:
					concatenated_frame = cframe["data"]
					if DEBUG:
						log_message = "Insert to NODES(NEW) : {frame}".format(frame=concatenated_frame)
						logger.info(log_message)
					tbl_to_insert = "nodes"
					conditions = None
					blank_schema = True
					insert_into_table(	schema=schema_map,
								dbname="oneems",
								tblname="nodes",
								updates=concatenated_frame,
								conditions=conditions,												
								ignores=["id"],
								blank_schema=blank_schema 	)
		
		"""
		elif flag == "C":
			tbl_to_insert = "discoveryres"
			conditions = {"deviceIpAddr":[str(xaddr)]}
			blank_schema = False
			node_frame = get_table_schema(tblname="nodes",blank=True)
			if node_frame["status"]:
				node_frame = node_frame["data"]
				cframe = mix_frames(frameA=node_frame,frameB=concatenated_frame,labels=["submarket","market","deviceos","devicename","nodeVersion","csr_site_name","csr_site_id","region","model"],updates={"deviceIpAddr":str(xaddr)})
				if cframe["status"]:
					concatenated_frame = cframe["data"]
					if DEBUG:
						log_message =  "update to NODES(CONFLILCT) : {frame}".format(frame=concatenated_frame)
						logger.info(log_message)
					tbl_to_insert = "nodes"
					#conditions = None
					#blank_schema = True
					insert_into_table(	schema=schema_map,
										dbname="oneems",
										tblname="nodes",
										updates=concatenated_frame,
										conditions=conditions,												
										ignores=["id"],
										blank_schema=blank_schema 	)
				
		
		else:
			tbl_to_insert = "discoveryres"
			conditions = {"deviceIpAddr":[str(xaddr)]}
			#blank_schema = False
			if flag == "K":
				#node_frame = get_table_schema(tblname="nodes",blank=True)
				#if node_frame["status"]:
					#node_frame = node_frame["data"]
				cframe = mix_frames(frameA=node_frame,frameB=concatenated_frame,labels=["lastpolled"],updates={"deviceIpAddr":str(xaddr)})
				if cframe["status"]:
					concatenated_frame = cframe["data"]
					if DEBUG:
						log_message =  "update to NODES(OK) : {frame}".format(frame=concatenated_frame)
						logger.info(log_message)
					tbl_to_insert = "nodes"
					#conditions = None
					blank_schema = False
					insert_into_table(	schema=schema_map,
										dbname="oneems",
										tblname="nodes",
										updates=concatenated_frame,
										conditions=conditions,
										ignores=["id"],
										blank_schema=blank_schema 	) """


if AS_API:
	walk_api = connexion.App(__name__)
	walk_api.add_api('snmp_walk.yaml')

	# application = app.app
	if __name__ == '__main__':
	    walk_api.run(port=8080)
else:
	snmp_api("opw")

