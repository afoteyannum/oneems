import re
from log_ import logger
from dynamic_mapper import 	fetch_from_db,\
							DEBUG
from ip_range_calculate import calculate_subnets

def is_valid_ipv4(ip):
    pattern = re.compile(r"""
        ^
        (?:
          (?:
            # Decimal 1-255 (no leading 0's)
            [3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
          |
            0x0*[0-9a-f]{1,2}  # Hexadecimal 0x0 - 0xFF (possible leading 0's)
          |
            0+[1-3]?[0-7]{0,2} # Octal 0 - 0377 (possible leading 0's)
          )
          (?:                  
            \.
            (?:
              [3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
            |
              0x0*[0-9a-f]{1,2}
            |
              0+[1-3]?[0-7]{0,2}
            )
          ){0,3}
        |
          0x0*[0-9a-f]{1,8}    
        |
          0+[0-3]?[0-7]{0,10}  
        |
          429496729[0-5]|42949672[0-8]\d|4294967[01]\d\d|429496[0-6]\d{3}|
          42949[0-5]\d{4}|4294[0-8]\d{5}|429[0-3]\d{6}|42[0-8]\d{7}|
          4[01]\d{8}|[1-3]\d{0,9}|[4-9]\d{0,8}
        )
        $
    """, re.VERBOSE | re.IGNORECASE)
    return pattern.match(ip) is not None

def get_ipallocation(market):
	devices = fetch_from_db(tblname="ipallocation",filter_={"market":market})
	result = {"status":False,"subnets":[]}
	for device in devices["data"]:
		ip_address = device["subnetmask"].split("/")[0].strip()
		flag_ip_addr_valid = is_valid_ipv4(ip_address)
		if DEBUG:
			log_message = "{ipaddr} is valid : {flag}".format(	ipaddr=str(ip_address), flag=str(flag_ip_addr_valid))
			logger.info(log_message)
		if flag_ip_addr_valid:
			result["status"] = True
			result["subnets"].append(device["subnetmask"])
	return result

def get_ipaddrs(market):
  result = {"status":False,"ipaddrs":[]}
  results = get_ipallocation(market)
  if results["status"]:
    if DEBUG:
      log_message = "ipallocation.py results : {results}".format(results=results)
      logger.info(log_message)
    subnets = results["subnets"]
    subs =  calculate_subnets(subnets)
    print subs
    if subs["status"]:
      ipaddrs = subs["ipaddrs"]
      if DEBUG:
        log_message = "ipallocation.py results[subnets]: {results}".format(results=ipaddrs)
        logger.info(log_message)
      result["status"] = True
      result["ipaddrs"] = ipaddrs
  else:
    if DEBUG:
      log_message = "ipallocation.py no result returned : {results}".format(results=results)
      logger.info(log_message)
  return result

