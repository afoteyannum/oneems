import ipcalc
from log_ import logger
from dynamic_mapper import 	DEBUG

def calculate_subnets(subnets):
	result = {"status":False,"ipaddrs":[]}
	if subnets:
		for subnet in subnets:
			for xaddr in ipcalc.Network(subnet):
				xaddr = str(xaddr)
				if DEBUG:
					log_message = "ip_range_calculate.py ipaddr : {ipaddr}".format(ipaddr=str(xaddr))
					logger.info(log_message)
				result["status"] = True
				result["ipaddrs"].append(xaddr)
	else:
		logger.info("[x] ip_range_calculate.py -- subnets = []")
	
  	return result

