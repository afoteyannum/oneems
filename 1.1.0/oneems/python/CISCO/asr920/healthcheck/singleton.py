class Singleton(type):
    """
    metaclass is the class of a class; that is, a class is an instance of its metaclass.
    You find the metaclass of an object in Python with type(obj).
    Normal new-style classes are of type type.
    """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]