import re
from singleton import Singleton

class ASR5000:
    """
    ASR5000 Class is specifically implemented for the ASR5X devices this class houses all the 
    routines to extract data from the log output.
    """

    __metaclass__ = Singleton #Singleton class meta for making class singleton in nature

    SHOW_CLOCK_TEMPLATE = r'show clock'
    EXTRACT_CLOCK_TEMPLATE = r'(.+?) (.+?) (.+?) (.+?) UTC (.+?)'
    SHOW_SYSTEM_UPTIME = r'show system uptime'
    SHOW_SYSTEM_UPTIME_EXTRACT = r'System uptime: (.+?) (.+?) (.+?)'
    SHOW_VERSION_GREP_IMAGE_VERSION_TEMPLATE = r'show version | grep "Image Version"'
    SHOW_VERSION_GREP_IMAGE_VERSION_EXTRACT = r'Image Version: 18.1.0'
    SHOW_SRP_INFO_GREP_CHASSIS_STATE_TEMPLATE = r'show srp info | grep "Chassis State"'
    SHOW_SRP_INFO_GREP_CHASSIS_STATE_EXTRACT = r'Chassis State: (.+?)'
    SHOW_HD_RAID_GREP_DEGRADE_TEMPLATE = r'show hd raid | grep "Degrad"'
    SHOW_HD_RAID_GREP_DEGRADE_EXTRACT = r'Degraded : (.+?)'
    SHOW_CONTEXT_TEMPLATE = r'show context'
    SHOW_SERVICE_ALL_TEMPLATE = r'show service all'
    SHOW_CARD_HARDWARE_GREP_PROG = r'show card hardware | grep Prog'
    SHOW_CARD_INFO_GREP_CARD_LOCK = r'show card info | grep "Card Lock"'
    SHOW_SESSION_RECOVERY_STARUS_VERBOSE = r'show session recovery status verbose'
    SHOW_RESOURCE_GREP_LICENSE = r'show resource | grep License'
    SHOW_LICENSE_INFO_GREP_LICENSE_STATUS = r'show license info | grep "License Status"'
    SHOW_SRP_CHECKPOINT_STATISTICS_GREP_SESSMGRS = r'show srp checkpoint statistics | grep Sessmgrs'
    SHOW_SRP_INFO = r'show srp info'
    SHOW_CARD_TABLE_GREP = r'show card table | grep -E -v "Active|Standby|None"'
    SHOW_DIAMETER_PEERS = r'show diameter peers full | grep "Total peers"'
    EXTRACT_DIAMETER_PEERS = r'Total peers matching specified criteria: (.+)'
    SHOW_CRASH_LIST = r'show crash list'
    EXTRACT_CRASH_LIST = r'Total Crashes : (.+?)'
    SHOW_RCT_STATUS = r'show rct stats'
    EXTRACT_RCT_MIGRATIONS = r'Migrations = (.+?)'
    SHOW_TASK_DIAMPROXY = r'show task resources | grep diamproxy'
    SHOW_TASK_SESSMG = r'show task resources | grep sessmg'
    SHOW_TASK_RESOURCE_GREP_V_GOOD = r'show task resources | grep -v good'
    SHOW_ALARM_OUTSTANDING = r'show alarm outstanding'

    def index_processor(self, Q, P):
        """
        Index processor finds the string approx matches and return there
        corresponding index position in the list of string when compared to the
        builtin `in` directive which is a match of keyword the index processor
        matches the value to maximum match and return the index based on
        maximum match.

        ex : string = 'hello world this is a test string#\nhello this is another str to match#'
        index_processor('hello this',string.split('\n'))
        output > 1

        :param Q: Query which has to match the list of strings.
        :param P: List of all the string.
        :return: Index of the maximum match
        """
        indexes = []
        index_to_use = None
        for idx, line in enumerate(P):
            if Q in line:
                indexes.append([idx, line])

        for match in indexes:
            string = match[-1]
            index = match[0]

            test_mts = string.replace(Q, '').replace('\r\n', '').strip().split('#')
            if len(test_mts) == 2:
                if test_mts[-1] == '':
                    index_to_use = index
                    break
        return index_to_use

    def segment_extract(self, SEGMENT, string_to_search, bounce=0):
        """
        Segment extract extract the segment required based on the
        regular expression pattern this further assistes in extracting
        data points of interest.

        :param SEGMENT: regex of the segment user need to extract
        :param string_to_search: string from the the SEGMENT is extracted
        :param bounce:  Incase of Unwanted data you need to skip some line
                        in the output
        :return: list of match data
        """
        regex = re.compile(SEGMENT)
        line_data = []
        found = False
        first_occurence = True
        for line in string_to_search:
            if regex.search(line) or found:
                found = True
                if not first_occurence:
                    if (line.find("#") != -1):
                        if (bounce == 0):
                            break
                        else:
                            bounce -= 1
                    else:
                        line_x = line.strip()
                        line_x = " ".join(line_x.split())
                        line_data.append(line_x)
                first_occurence = False
        return line_data

    def extract_show_clock(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show clock` using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: show clock output
        """
        line_data = self.segment_extract(ASR5000.SHOW_CLOCK_TEMPLATE, string_to_search)
        return_dict = {'show_clock': None}
        for line in line_data:
            m = re.search(ASR5000.EXTRACT_CLOCK_TEMPLATE, line)
            if m:
                return_dict['show_clock'] = line.strip()
                break
        return return_dict

    def extract_show_system_uptime(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show system uptime` using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: show system uptime output
        """
        line_data = self.segment_extract(ASR5000.SHOW_SYSTEM_UPTIME, string_to_search)
        return_dict = {'show_system_uptime': None}
        for line in line_data:
            m = re.search(ASR5000.SHOW_SYSTEM_UPTIME_EXTRACT, line)
            if m:
                return_dict['show_system_uptime'] = line.split(':')[-1].strip()
                break
        return return_dict

    def extract_show_version_grep_image_version(self,
                                                string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show version | grep "Image Version"`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: show version image version output
        """
        line_data = self.segment_extract(ASR5000.SHOW_VERSION_GREP_IMAGE_VERSION_TEMPLATE, string_to_search)
        return_dict = {'show_version_grep_image_version': None}
        for line in line_data:
            m = re.search(ASR5000.SHOW_VERSION_GREP_IMAGE_VERSION_EXTRACT, line)
            if m:
                return_dict['show_version_grep_image_version'] = line.split()[-1].strip()
                break
        return return_dict

    def show_srp_info_grep_chassis_state(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show srp info | grep "Chassis State"`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: chassis state info output
        """
        index_to_use = self.index_processor(ASR5000.SHOW_SRP_INFO_GREP_CHASSIS_STATE_TEMPLATE, string_to_search)
        line_data = self.segment_extract(ASR5000.SHOW_SRP_INFO_GREP_CHASSIS_STATE_TEMPLATE,
                                         string_to_search[index_to_use:])
        return_dict = {'show_srp_info_grep_chassis_state': None}
        for line in line_data:
            m = re.search(ASR5000.SHOW_SRP_INFO_GREP_CHASSIS_STATE_EXTRACT, line)
            if m:
                return_dict['show_srp_info_grep_chassis_state'] = line.split()[-1].strip()
                break
        return return_dict

    def show_hd_raid_grep_degrade(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show hd raid | grep "Degrad"`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: hd raid degrad output
        """
        line_data = self.segment_extract(ASR5000.SHOW_HD_RAID_GREP_DEGRADE_TEMPLATE, string_to_search)
        return_dict = {'show_hd_raid_grep_degrade': None}
        for line in line_data:
            m = re.search(ASR5000.SHOW_HD_RAID_GREP_DEGRADE_EXTRACT, line)
            if m:
                return_dict['show_hd_raid_grep_degrade'] = line.split()[-1].strip()
                break
        return return_dict

    def extract_show_context(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show context` using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: show context active output count
        """
        line_data = self.segment_extract(ASR5000.SHOW_CONTEXT_TEMPLATE, string_to_search)
        return_dict = {'active_count': 0, 'R': 0}
        start_label = '---------' #This sequence represents the start of the count
        blk = True
        start = False
        total_lines = 0
        for line in line_data:
            if line:
                if start:
                    total_lines += 1
                    if 'Active' in line.split():
                        return_dict['active_count'] += 1
                if blk:
                    if start_label in line:
                        start = True
                        blk = False
        if not ((total_lines - return_dict['active_count']) == 0):
            return_dict['R'] = 1
        return return_dict

    def extract_show_service_all(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show service all` using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: unique context id count,unique service id count
        """
        line_data = self.segment_extract(ASR5000.SHOW_SERVICE_ALL_TEMPLATE, string_to_search)
        start_label = '-----------'#This sequence represents the start of the count
        start = False
        ucid = {}
        usid = {}
        for line in line_data:
            if line:
                if start:
                    data = line.split()
                    if len(data) > 2:
                        try:
                            cid = int(data[0])
                            sid = int(data[1])
                            if cid in ucid:
                                ucid[cid] += 1
                            else:
                                ucid[cid] = 1

                            if sid in usid:
                                usid[sid] += 1
                            else:
                                usid[sid] = 1
                        except ValueError as e:
                            print '[x] Error occured for type conversion from ( assumend int as )char*->int'
                            pass
                else:
                    if line.split()[2] == start_label:
                        start = True
        return {'count_unique_context_id': len(ucid), 'count_unique_service_id': len(usid)}

    def extract_show_card_hardware_grep_prog(self,
                                             string_to_search=open('asr5000_health_check_outputs.txt').readlines()):

        """
        extracts the output for the command `show card hardware | grep Prog`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: up to date count
        """
        line_data = self.segment_extract(ASR5000.SHOW_CARD_HARDWARE_GREP_PROG, string_to_search)
        return_dict = {'upto_date_count': 0, 'R': 0}
        start = False
        blk = True
        total_lines = 0
        for line in line_data:
            if blk:
                if 'Card Programmables' in line:#This sequence represents the start of the count
                    start = True
                    blk = False
            if start:
                if line:
                    total_lines += 1
                    data_pre = [_.strip() for _ in line.split(':')]
                    if 'up to date' in data_pre:
                        return_dict['upto_date_count'] += 1
        if not ((total_lines - return_dict['upto_date_count']) == 0):
            return_dict['R'] = 1
        return return_dict

    def extract_show_card_info_grep_card_lock(self,
                                              string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show card info | grep "Card Lock"`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: locked + unlocked card count
        """
        line_data = self.segment_extract(ASR5000.SHOW_CARD_INFO_GREP_CARD_LOCK, string_to_search)
        return_dict = {'locked_count': 0, 'unlocked_count': 0}
        for line in line_data:
            if 'Locked' in line:
                return_dict['locked_count'] += 1
            if 'Unlocked' in line:
                return_dict['unlocked_count'] += 1
        return return_dict

    def extract_show_session_recovery_status_verbose(self, string_to_search=open(
        'asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show session recovery status verbose`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: overall status
        """
        line_data = self.segment_extract(ASR5000.SHOW_SESSION_RECOVERY_STARUS_VERBOSE, string_to_search)
        return_dict = {'overall_status': None}
        for line in line_data:
            if 'Overall Status' in line:
                return_dict['overall_status'] = line.split(':')[-1].strip()
                break
        return return_dict

    def extract_show_resource_grep_license(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show resource | grep License`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: acceptable limit count
        """
        line_data = self.segment_extract(ASR5000.SHOW_RESOURCE_GREP_LICENSE, string_to_search)
        start = False
        blk = True
        total_lines = 0
        return_dict = {'within_acceptable_limits_count': 0, 'R': 0}
        for line in line_data:
            data_pre = [_.strip() for _ in line.split(':')]
            if blk:
                if 'License Status' in data_pre:
                    start = True
                    blk = False
            if start:
                if line:
                    total_lines += 1
                    if 'Within Acceptable Limits' in data_pre:
                        return_dict['within_acceptable_limits_count'] += 1
        if not ((total_lines - return_dict['within_acceptable_limits_count']) == 0):
            return_dict['R'] = 1
        return return_dict

    def extract_show_license_info_grep_license_status(self, string_to_search=open(
        'asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show license info | grep "License Status"`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: License status
        """
        line_data = self.segment_extract(ASR5000.SHOW_LICENSE_INFO_GREP_LICENSE_STATUS, string_to_search)
        return_dict = {'license_status': None}
        for line in line_data:
            if 'License Status' in line:
                return_dict['license_status'] = line.replace('License Status', '').strip()
                break
        return return_dict

    def extract_show_srp_checkpoint_statistics_grep_sessmgrs(self, string_to_search=open(
        'asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show srp checkpoint statistics | grep Sessmgrs`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: Count of Sessmgrs
        """
        line_data = self.segment_extract(ASR5000.SHOW_SRP_CHECKPOINT_STATISTICS_GREP_SESSMGRS, string_to_search)
        return_dict = {'number_of_sessmgrs': None}
        for line in line_data:
            if 'Number of Sessmgrs' in line:
                return_dict['number_of_sessmgrs'] = line.split(':')[-1].strip()
                break
        return return_dict

    def extract_show_srp_info(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show srp info` using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: chassis state
        """
        index_to_use = self.index_processor(ASR5000.SHOW_SRP_INFO, string_to_search)
        line_data = self.segment_extract(ASR5000.SHOW_SRP_INFO, string_to_search[index_to_use:])
        return_dict = {'chassis_state': None}
        for line in line_data:
            if 'Chassis State' in line:
                data = line.split(':')
                if len(data) == 2:
                    return_dict['chassis_state'] = data[-1]
                    break
        return return_dict

    def extract_show_card_table_grep(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show card table | grep -E -v "Active|Standby|None"`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: card count
        """
        index_to_use = self.index_processor(ASR5000.SHOW_CARD_TABLE_GREP, string_to_search)
        line_data = self.segment_extract(ASR5000.SHOW_CARD_TABLE_GREP, string_to_search[index_to_use:])
        match = '--------------------------------------'#This sequence represents the start of the count
        start = False
        blk = True
        return_dict = {'count': 0}
        for line in line_data:
            data = line.split()
            if start:
                if line:
                    return_dict['count'] += 1
            if blk:
                if len(data) > 2:
                    if data[1] == match:
                        start = True
                        blk = False
        return return_dict

    def extract_show_diameter_peers(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show diameter peers full | grep "Total peers"`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: card count
        """
        line_data = self.segment_extract(ASR5000.SHOW_DIAMETER_PEERS, string_to_search)
        for line in line_data:
            m = re.search(ASR5000.EXTRACT_DIAMETER_PEERS, line)
            if m:
                return {'total_peers': m.group(1)}

    def extract_show_crash_list(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show crash list` using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: total crashes
        """
        line_data = self.segment_extract(ASR5000.SHOW_CRASH_LIST, string_to_search, bounce=1)
        for line in line_data:
            m = re.search(ASR5000.EXTRACT_CRASH_LIST, line)
            if m:
                return {"total_crashes": m.group(1)}

    def extract_show_rct_status(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show rct stats` using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: migrations in the rct status
        """
        line_data = self.segment_extract(ASR5000.SHOW_RCT_STATUS, string_to_search)
        for line in line_data:
            m = re.search(ASR5000.EXTRACT_RCT_MIGRATIONS, line)
            if m:
                return {"migrations": m.group(1)}

    def extract_show_task_diamproxy(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show task resources | grep diamproxy`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: migrations in the rct status
        """
        index_to_use = self.index_processor(ASR5000.SHOW_TASK_DIAMPROXY, string_to_search)
        line_data = self.segment_extract(ASR5000.SHOW_TASK_DIAMPROXY, string_to_search[index_to_use:])
        cnt = 0
        for line in line_data:
            if 'good' in line:
                cnt += 1
        return {"count": cnt}

    def extract_show_task_sessmg(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show task resources | grep sessmg`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: migrations in the rct status
        """
        index_to_use = self.index_processor(ASR5000.SHOW_TASK_SESSMG, string_to_search)
        line_data = self.segment_extract(ASR5000.SHOW_TASK_SESSMG, string_to_search[index_to_use:])
        cnt = 0
        e = 0
        for line in line_data:
            if line:
                e += 1
                if 'good' in line:
                    cnt += 1
        if e - cnt > 1:
            return {'count': cnt, 'status': 'fail'}
        else:
            return {'count': cnt, 'status': 'pass'}

    def extract_show_task_resource_grep_v_good(self,
                                               string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show task resources | grep -v good`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: count of all the resources that are flagged `good`
        """
        line_data = self.segment_extract(ASR5000.SHOW_TASK_RESOURCE_GREP_V_GOOD, string_to_search)
        match = '---------'#This sequence represents the start of the count
        start = False
        blk = True
        return_dict = {'count': 0}
        for line in line_data:
            data = line.split()
            if start:
                if line:
                    return_dict['count'] += 1
            if blk:
                if len(data) > 2:
                    if data[1] == match:
                        start = True
                        blk = False
        if return_dict['count'] > 1:
            return_dict['R'] = 1
        else:
            return_dict['R'] = 0
        return return_dict

    def extract_show_alarm_outstanding(self, string_to_search=open('asr5000_health_check_outputs.txt').readlines()):
        """
        extracts the output for the command `show alarm outstanding`
        using the ::segment_extract.

        :param string_to_search: console output as list of strings.
        :return: count of all the outstanding alarms.
        """
        line_data = self.segment_extract(ASR5000.SHOW_ALARM_OUTSTANDING, string_to_search)
        match = '----------'#This sequence represents the start of the count
        start = False
        blk = True
        return_dict = {'count': 0}
        for line in line_data:
            data = line.split()
            if start:
                if line:
                    return_dict['count'] += 1
            if blk:
                if len(data) > 2:
                    if data[1] == match:
                        start = True
                        blk = False
        if return_dict['count'] > 1:
            return_dict['R'] = 1
        else:
            return_dict['R'] = 0
        return return_dict

    def asr5000_show_clock(self, output, response):
        resp = self.extract_show_clock()  # output.split("\n")

        response["show_clock"] = resp.get("show_clock", "")

    def asr5000_show_system_uptime(self, output, response):
        resp = self.extract_show_system_uptime()  # output.split("\n")
        response["show_system_uptime"] = resp.get("show_system_uptime", "")

    def asr5000_show_version_grep_img_ver(self, output, response):
        resp = self.extract_show_version_grep_image_version()  # output.split("\n")
        response["show_version_grep_image_version"] = resp.get("show_version_grep_image_version", "")

    def asr5000_show_srp_info_grep_chassis_state(self, output, response):
        resp = self.show_srp_info_grep_chassis_state()  # output.split("\n")
        response["show_srp_info_grep_chassis_state"] = resp.get("show_srp_info_grep_chassis_state", "")

    def asr5000_show_hd_raid_grep_degrad(self, output, response):
        resp = self.show_hd_raid_grep_degrade()  # output.split("\n")
        response["show_hd_raid_grep_degrad"] = resp.get("show_hd_raid_grep_degrade", "")

    def asr5000_show_context(self, output, response):
        resp = self.extract_show_context(output.split("\n"))
        response["show_context"] = resp

    def asr5000_show_service_all(self, output, response):
        resp = self.extract_show_service_all(output.split("\n"))
        response["show_service_all"] = resp

    def asr5000_show_card_hardware_grep_prog(self, output, response):
        resp = self.extract_show_card_hardware_grep_prog(output.split("\n"))
        response["show_card_hardware_grep_prog"] = resp

    def asr5000_show_card_info_grep_card_lock(self, output, response):
        resp = self.extract_show_card_info_grep_card_lock(output.split("\n"))
        response["show_card_info_grep_card_lock"] = resp

    def asr5000_session_recovery_status_verbose(self, output, response):
        resp = self.extract_show_session_recovery_status_verbose(output.split("\n"))
        response["show_session_recovery_status_verbose"] = resp

    def asr5000_show_resource_grep_license(self, output, response):
        resp = self.extract_show_resource_grep_license(output.split("\n"))
        response["show_resource_grep_license"] = resp

    def asr5000_show_license_info_grep_license_status(self, output, response):
        resp = self.extract_show_license_info_grep_license_status(output.split("\n"))
        response["show_license_info_grep_license_status"] = resp

    def asr5000_show_srp_checkpoint_statistics_grep_sessmgrs(self, output, response):
        resp = self.extract_show_srp_checkpoint_statistics_grep_sessmgrs(
            output.split("\n"))  # output.split("\n")
        response["show_srp_checkpoint_statistics_grep_sessmgrs"] = resp

    def asr5000_show_srp_info(self, output, response):
        resp = self.extract_show_srp_info(output.split("\n"))
        response["show_srp_info"] = resp

    def asr5000_show_card_table_grep_act_stdby(self, output, response):
        resp = self.extract_show_card_table_grep(output.split("\n"))
        response["show_card_table_grep"] = resp

    def asr5000_show_diameter_peers_full_grep_total_peers(self, output, response):
        resp = self.extract_show_diameter_peers(output.split("\n"))
        response["show_diameter_peers"] = resp

    def asr5000_show_crash_list(self, output, response):
        resp = self.extract_show_crash_list(output.split("\n"))
        response["totalcrashes"] = resp

    def asr5000_show_rct_stats(self, output, response):
        resp = self.extract_show_rct_status(output.split("\n"))
        response["show_rct_status"] = resp

    def asr5000_show_task_resources_grep_diamproxy(self, output, response):
        resp = self.extract_show_task_diamproxy(output.split("\n"))
        response["diamproxy"] = resp

    def asr5000_show_task_resources_grep_sessmg(self, output, response):
        resp = self.extract_show_task_sessmg(output.split("\n"))
        response["show_task_resources_grep_sessmg"] = resp

    def asr5000_show_task_resources_grep_v_good(self, output, response):
        resp = self.extract_show_task_resource_grep_v_good(output.split("\n"))
        response['show_task_resource_grep_v_good'] = resp

    def asr5000_show_alarm_outstanding(self, output, response):
        resp = self.extract_show_alarm_outstanding(output.split("\n"))
        response['show_alarm_outstanding'] = resp



asr5000_obj = ASR5000()
