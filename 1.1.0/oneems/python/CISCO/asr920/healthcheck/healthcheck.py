# Importing necessary libraries
import re
import time
import paramiko
import connexion
from userdevices import get_node
from socket import error as Serror
from command_mapper import switch, switch_numeric, ios_obj
from paramiko.ssh_exception import AuthenticationException, NoValidConnectionsError


# -------------------------------

# Turning on debug - to be removed later
DEBUG = True
# ------------------

SESSION = None
# constructing ping commands with parameters
PING_COMMAND_2000 = r'ping (.+?) size 2000'
PING_COMMAND_5000 = r'ping (.+?) size 5000'
# -------------------------------------------

def show_inventory(output, response):
    global SESSION
    if SESSION:
        connection = SESSION
        ios_obj.extract_show_inventory()  # output.split("\n")
        ios_obj.form_command_idprom_detail()
        map_dict = {}
        for cmd in ios_obj.IDPROM_DETAIL:
            connection.send("terminal length 0")
            connection.send("\n")
            connection.send(cmd)
            connection.send("\n")
            time.sleep(1.0)
            result = ios_obj.extract_idprom_details(query=cmd)  # output_intermediate.split("\n")
            if True:
                map_dict[cmd] = result
        for cmd in ios_obj.STATUS:
            connection.send("terminal length 0")
            connection.send("\n")
            connection.send(cmd)
            connection.send("\n")
            time.sleep(1.0)
            iresp = {cmd: {}}
            result = ios_obj.extract_tranciever_status(query=cmd)  # output_intermediate.split("\n")
            if result:
                key_to_check = cmd.replace('status', 'idprom detail')
                if map_dict.has_key(key_to_check):
                    data = map_dict.get(key_to_check, None)
                    if data:
                        resp = ios_obj.temperature_range_check(data, result)
                        if resp:
                            iresp[cmd].update(resp)
                        resp = ios_obj.supply_voltage_range_check(data, result)
                        if resp:
                            iresp[cmd].update(resp)
                        resp = ios_obj.tx_power(data, result)
                        if resp:
                            iresp[cmd].update(resp)
                        resp = ios_obj.rx_power(data, result)
                        if resp:
                            iresp[cmd].update(resp)
            response.update(iresp)

def show_ping_2000(output,response,_raw=False):
    global SESSION
    connection = SESSION
    connection.send("terminal length 0")
    connection.send("\n")
    connection.send("show ip bgp vpnv4 all summary")
    connection.send("\n")
    time.sleep(3.0)
    output_intermediate = connection.recv(65535)
    neighbours = ios_obj.bgpv4_neighbour(output_intermediate.split("\n"))
    commands = ios_obj.ping_extract(bgpv4_neighbour=neighbours,type_='2000')
    print commands
    raw_output = {}
    for command in commands:
        if commands[command]:
            for cmd in commands[command]:
                message = cmd
                connection.send("terminal length 0")
                connection.send("\n")
                connection.send(cmd)
                connection.send("\n")
                time.sleep(1.0)
                output_intermediate = connection.recv(65535)
                print output_intermediate
                print "[i] output : {x}".format(x=output_intermediate)
                if _raw:
                    raw_output[message] = output_intermediate.replace('\r\n', '<br>')

                m = re.search(PING_COMMAND_2000,message)
                if m:
                    resp = ios_obj.extract_2000_5000(message, output_intermediate.split("\n"))
                    sc_rate = resp.get("sucess","")
                    if sc_rate:
                        twothsndbyteping = {"twothsndbyteping":{
                                                 "R": 0,
                                                "message": "success rate = %s percent"%(str(sc_rate),)
                                                }
                                            }
                    else:
                        twothsndbyteping = {"twothsndbyteping":{
                                                 "R": 1,
                                                 "message": None,
                                                }
                                            }
                    if not _raw:
                        response.update(twothsndbyteping)
    if _raw:
        return raw_output

def show_ping_5000(output,response,_raw=False):
    global SESSION
    connection = SESSION
    connection.send("terminal length 0")
    connection.send("\n")
    connection.send("show ip bgp vpnv4 all summary")
    connection.send("\n")
    time.sleep(3.0)
    output_intermediate = connection.recv(65535)
    neighbours = ios_obj.bgpv4_neighbour(output_intermediate.split("\n"))
    commands = ios_obj.ping_extract(bgpv4_neighbour=neighbours,type_='5000')
    print commands
    raw_output = {}
    for command in commands:
        if commands[command]:
            for cmd in commands[command]:
                message = cmd
                connection.send("terminal length 0")
                connection.send("\n")
                connection.send(cmd)
                connection.send("\n")
                time.sleep(1.0)
                output_intermediate = connection.recv(65535)
                print output_intermediate
                print "[i] output : {x}".format(x=output_intermediate)
                if _raw:
                    raw_output[message] = output_intermediate.replace('\r\n', '<br>')
                m = re.search(PING_COMMAND_5000, message)
                if m:
                    resp = ios_obj.extract_2000_5000(message)
                    sc_rate = resp.get("sucess","")
                    if sc_rate:
                        fivethsndbyteping = {"fivethsndbyteping":{
                                                     "R": 0,
                                                     "message": "success rate = %s percent"%(str(sc_rate),)
                                                    }
                                                }
                    else:
                        fivethsndbyteping = {"fivethsndbyteping":{
                                                     "R": 1,
                                                     "message": None
                                                    }
                                                }
                    if not _raw:
                        response.update(fivethsndbyteping)
    if _raw:
        return raw_output

switch["ios"]["show inventory"] = show_inventory
switch["ios"]["show ping 2000"] = show_ping_2000
switch["ios"]["show ping 5000"] = show_ping_5000

# Get the node's list and pass the details to SSH for the given User_id
def perform_hc(device_type, device_id):
    switch_mapper = switch.get(device_type, None)
    if switch_mapper:
        nlist = get_node(device_id, device_type)
        if nlist:
            return ssh_post(nlist, switch_mapper, device_type)
    return {}


# ----------------------------------------

def perform_hc_full(device_type, device_id, command):
    switch_mapper = switch.get(device_type, None)
    nlist = get_node(device_id, device_type)
    command_string = switch_numeric.get(device_type, '').get(int(command), '')
    if command_string:
        nlist['devices'][0]['command'] = [command_string]
        if nlist:
            return [{command_string: ssh_post(nlist, switch_mapper, device_type, raw_=True)}]
    return {}



#x="THIS IS A PRIVATE NETWORK AND COMPUTING SYSTEM FOR EXCLUSIVE USE BY AUTHORIZED PERSONNEL ONLY.You are requesting access to a private network and computing system for exclusive use by authorized persons only. If you have not been expressly authorized to access this system by Verizon Wireless, please log off immediately, otherwise your actions may be subject to civil and/or criminal penalties including but not limited to excessive fines and imprisonment. NOTICE TO ALL AUTHORIZED USERS: This is a private and proprietary system owned, operated and monitored by Verizon Wireless. Use of this system and any data in transit to/from or stored on this system can be accessed, intercepted, monitored, recorded, copied, audited and inspected by Verizon Wireless and disclosed to third parties and/or law enforcement. Your use of this system constitutes consent to such monitoring, disclosure and associated activities. Any use that exceeds authorization or is improper, unlawful, unintended or not permitted (such as attempts to upload or change information; to defeat or circumvent security features; or to utilize this system other than for its intended purpose) is prohibited and may result in immediate revocation of access privileges by Verizon Wireless and civil and/or criminal prosecution.By continuing to use this system you agree to comply with all applicable policies and procedures and consent to the terms and conditions contained herein. If you do not agree, please log off immediately."
x="THIS IS A PRIVATE NETWORK AND COMPUTING SYSTEM FOR EXCLUSIVE USE BY AUTHORIZED"
y="\r\n\r\nPERSONNEL ONLY.\r\nYou are requesting access to a private network and computing system for\r\nexclusive use by authorized persons only. If you have not been expressly\r\nauthorized to access this system by Verizon Wireless, please log off\r\nimmediately, otherwise your actions may be subject to civil and/or criminal\r\npenalties including but not limited to excessive fines and imprisonment.\r\nNOTICE TO ALL AUTHORIZED USERS:\r\nThis is a private and proprietary system owned, operated and monitored by\r\nVerizon Wireless. Use of this system and any data in transit to/from or stored\r\non this system can be accessed, intercepted, monitored, recorded, copied,\r\naudited and inspected by Verizon Wireless and disclosed to third parties and/or\r\nlaw enforcement. Your use of this system constitutes consent to such\r\nmonitoring, disclosure and associated activities. Any use that exceeds\r\nauthorization or is improper, unlawful, unintended or not permitted (such as\r\nattempts to upload or change information; to defeat or circumvent security\r\nfeatures; or to utilize this system other than for its intended purpose) is\r\nprohibited and may result in immediate revocation of access privileges by\r\nVerizon Wireless and civil and/or criminal prosecution.\r\nBy continuing to use this system you agree to comply with all applicable\r\npolicies and procedures and consent to the terms and conditions contained\r\nherein. If you do not agree, please log off immediately.\r\n\r\n"

# Using the SSH Function
def ssh_post(data, switch_mapper, logic, raw_=False):
    # try:
    global SESSION
    # Using device details to execute HealthCheck's for each device from Device's list
    devices = data.get("devices")
    for device in devices:
        response = {}
        # start=time.time()
        ip_addr = device.get('ip_addr').strip()
        commands = device.get('command')
        username = device.get('username').strip()
        password = device.get('password').strip()
        session = paramiko.SSHClient()
        session.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            session.connect(ip_addr, username=username, password=password)
        except (AuthenticationException, Serror) as e:
            if e.message:
                return {"error": True, "message": "[x] ERROR: " + e.message}
            else:
                return {"error": True, "message": "[x] ERROR: " + e.strerror}
        except (NoValidConnectionsError) as err:
            if err:
                return {"error": True, "Message": err}
        connection = session.invoke_shell()
        SESSION = connection
        time_dict = {}
        # From commands list each command is passed to the SSH Shell terminal
        if ip_addr:
            if commands:
                grouped_commands = None
                for index, command in enumerate(commands):
                    if type(command) == dict:
                        grouped_commands = commands.pop(index)
                    else:
                        start = time.time()
                        connection.send("terminal length 0")
                        connection.send("\n")
                        message = command
                        connection.send(message)
                        connection.send("\n")
                        time.sleep(1.0)
                        output = connection.recv(65535)
                        sw_func = switch_mapper.get(message, None)
                        if raw_:
                            if message == "show ping 2000":
                                if sw_func:
                                    return sw_func(None, None, _raw=True)
                            if message == "show ping 5000":
                                if sw_func:
                                    return sw_func(None, None, _raw=True)
                            output1=output.replace(x,'')
                            output2=output1.replace(y,'')
                            output2=output2.replace('\t','')
                            SESSION.close()
                            return output2.replace('\r\n', '<br>')


                        if sw_func:
                            sw_func(output, response)
                        end = time.time()
                        time_dict[message] = end - start

                if grouped_commands:
                    grouped_commands = grouped_commands.get('grouped')
                    for group in grouped_commands:
                        resp = {}
                        start = time.time()
                        for command in grouped_commands[group]:
                            connection.send("terminal length 0")
                            connection.send("\n")
                            message = command  # " ".join(command.split("-"))
                            connection.send(message)
                            connection.send("\n")
                            time.sleep(1.0)
                            output = connection.recv(65535)
                            sw_func = switch_mapper.get(message, None)
                            if sw_func:
                                sw_func(output, resp)
                        end = time.time()
                        time_dict[group] = end - start
                        response[group] = resp
        SESSION.close()

    return response
    # except NoValidConnectionsError as err:
    # return {"message": "connection failed"}


healthcheck = connexion.App(__name__)
healthcheck.add_api('healthcheck.yaml')

# application = app.app
if __name__ == '__main__':
    healthcheck.run(port=8080)
