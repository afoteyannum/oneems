from database import database_obj

COMMAND_MAPPER = {'ios': [          'show processes cpu',
                                    'show memory statistics',
                                    'show buffers',
                                    'show version',
                                    'show running-config | i boot',
                                    'show version | i Config',
                                    'show environment',
                                    'show platform',
                                    'show bfd neighbor',
                                    'show ip interface brief',
                                    'show interfaces',
                                    'show mpls interfaces',
                                    'show mpls ldp neighbor',
                                    'show ip bgp vpnv4 all summary',
                                    'show ip bgp vpnv6 unicast all summary',
                                    'show ip bgp vpnv6 unicast vrf LTE',
                                    'show ping 2000',
                                    'show ping 5000',
                                    'show logging',
                                    'show ip bgp vpnv4 vrf RAN',
                                    'show xconnect all',
                                    {'grouped':{
                                        'show ip bgp vpnv4 vrf': [
                                            'show ip bgp vpnv4 vrf 1XRTT',
                                            'show ip bgp vpnv4 vrf RAN',
                                            'show ip bgp vpnv4 vrf CELL_MGMT'
                                        ],
                                        #'lightlevel':['show inventory']
                                    } }
                        ],

                'xos' : [       'show clock',
                                'show system uptime',
                                'show version | grep "Image Version"',
                                'show srp info | grep "Chassis State"',
                                'show hd raid | grep "Degrad"',
                                'show context',
                                'show service all',
                                'show card hardware | grep Prog',
                                'show card info | grep "Card Lock"',
                                'show session recovery status verbose',
                                'show resource | grep License',
                                'show license info | grep "License Status"',
                                'show srp checkpoint statistics | grep Sessmgrs',
                                'show srp info',
                                'show card table | grep -E -v "Active|Standby|None"',
                                'show diameter peers full | grep "Total peers"',
                                'show crash list',
                                'show rct stats',
                                'show task resources | grep diamproxy',
                                'show task resources | grep sessmg',
                                'show task resources | grep -v good',
                                'show alarm outstanding'
                            ]
            }


def get_node(id_,device_type):
    database_obj.cursor.execute( "select * from nodes where id=%s;" %(str(id_),))
    node = database_obj.return_result(database_obj.cursor)
    if node:
        node = node[0]
        if node:
            ip_addr = node[7]
            uname = node[2]
            psswrd = node[3]
            packet = {'ip_addr': ip_addr,
                      'username': uname,
                      'password': psswrd,
                      'command': COMMAND_MAPPER.get(device_type,[]) }
            node = packet
    return {"devices" :[node]}
