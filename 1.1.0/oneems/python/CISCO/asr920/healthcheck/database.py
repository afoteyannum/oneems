import MySQLdb
from singleton import Singleton

class DBCONN:

    __metaclass__ = Singleton

    def __init__(self):
        self.db = MySQLdb.connect( host="10.134.179.99",
                              user="reader",
                              passwd="redaer1103",
                              db="oneemstest")
        self.cursor  = self.db.cursor()

    def return_result(self,cursor):
        result = []
        for res in cursor.fetchall():
            result.append(res)
        return result

database_obj = DBCONN()