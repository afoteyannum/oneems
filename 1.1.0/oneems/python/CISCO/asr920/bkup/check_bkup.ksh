#!/bin/ksh

ps -ef | grep config_backup.py | grep -qv grep
res=$?
if [ $res == "1" ]
then
	current_dt=`date`
	print "$current_dt: Restarted config_backup"
	/bin/python /usr/apps/oneems/python/CISCO/asr920/bkup/config_backup.py >> /usr/apps/oneems/log/config_backup.log 2>&1
fi
