import os
import time
import stat
import paramiko
import connexion
import subprocess
from paramiko import SSHClient
from extractor import extractor_object
from dynamic_mapper import fetch_from_db

DEBUG = True

def get_result(**kwargs):
	command = kwargs.get("command",None)
	return_val = {"success":False,"data":{"success":None,
										  "error":None}}
	if command:
		proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
		return_val["success"] = True
		return_val["data"]["success"] = proc.stdout.read()
		if proc.stderr:
			return_val["data"]["error"] = proc.stderr.read()
	return return_val

def process_filenames_router(**kwargs):
	filenames = kwargs.get("filenames",None)
	return_val = {"success":False,"data":None}
	START_LISTING = "bootflash:/"
	START = False
	names = []
	if filenames:
		for filename in filenames.split("\r\n"):
			if filename:
				filename = filename.replace(" "*109,"").split(" ")[-1]
				if START:
					names.append(filename)
				else:
					if filename.strip() == START_LISTING:
						START = True 
		if names:
			return_val["success"] = True
			return_val["data"] = names
	return return_val

def file_compare(**kwargs):
	router_files = kwargs.get("router_files",None)
	server_files = kwargs.get("server_files",None)
	return_val = {"success":False,"data":{}}
	if ( server_files and router_files ):
		for file_ in router_files:
			file_to_query = file_
			if file_to_query in server_files:
				return_val["success"] = True
				return_val["data"][file_to_query] = "DEL"
	return return_val

def create_name(**kwargs):
	def kv_checker(kv,keys):
		all_available = []
		for key in keys:
			if kv[key].strip():
				all_available.append(True)
			else:
				all_available.append(False)
		return all(all_available)

	mapper = kwargs.get("mapper",None)
	values = kwargs.get("values",None)
	response = {"success":False,"data":None}
	if ( mapper and values ):
		name = ""
		keys__ = []
		for key in mapper:
			if type(key) == str:
				keys__.append(key)

		if kv_checker(kv=values,keys=keys__):
			for key in mapper:
				name_val = ""
				if type(key) == dict:
					name_val = key["extra"]
				else:
					if key in values:
						name_val = values[key]
				name += name_val.replace(" ","").replace("-","-").replace(":","_")+ "-"
			if name:
				response["success"] = True
				response["data"] = name[:-1]+".cfg"
		else:
			response["success"] = False
			response["data"] = "[x] Unable to PING device"
	return response

def snmp_walk(**kwargs):
	device_type = kwargs.get("device_type",None)
	ip_addr = kwargs.get("ipaddr",None)
	command_ = kwargs.get("command_",None)

	response = {"data": None,"success":False}
	command = "snmpwalk -v2c -c %s %s %s"%(str(device_type),str(ip_addr),str(command_))
	if DEBUG:
		print "[i] SNMP command : {command}".format(command=command)
	result = get_result(command=command)
	if result["success"]:
		exdata = extractor_object.run_all(result["data"]["success"])
		if exdata["status"]:
			response["success"] = True
			response["data"] = exdata["data"]
	return response

def exec_cmd(**kwargs):
	command = kwargs.get("command",None)
	sequences = kwargs.get("sequences",None)
	ipaddr = kwargs.get("ipaddr",None)
	username = kwargs.get("username",None)
	password = kwargs.get("password",None)

	response = {"success":False,"data":None}
	message = "[x] something went wrong"
	if sequences:
		if command:
			ssh = SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			if(ipaddr and username and password):
				ssh.connect(ipaddr,username=username,password=password)
				if DEBUG:
					print "[i] exec {command} on router ipaddr:{ipaddr}, username:{uname}, password:{pword}".format(command=command,ipaddr=ipaddr,uname=username,pword=password)
				stdin, stdout, stderr = ssh.exec_command(command)
				stdin.flush()
				for sequence in sequences:
					stdin.write(sequence)
					time.sleep(1)
				response["success"] = True
				response["data"] = {"output":stdout.read(),
									"error":stderr.read()}
			else:
				response["data"] = "[x] pass `ipaddr=<ipadddr>, username=<username>, password=<password>`"
		else:
			response["data"]  = "[x] use `exec_cmd(sequences=<list>,command=<cmd>`"
	else:
		response["data"]  = "[x] use `exec_cmd(sequences=<list>,command=<cmd>`"
	return response

def config_backup(**kwargs):
	response = {"success":False, "data":None}

	def key_checker(kw,keys):
		not_there = []
		for key in keys:
			if key not in kw.keys():
				not_there.append(key)
		return not_there

	not_there = key_checker(kw=kwargs,keys=["ipaddr","username","password",
											"command","device_type","oid",
											"server_path","router_path"])
	if not_there:
		msg = "[x] parameter not there in callable : `{keys}`".format(keys=" ".join(_ for _ in not_there))
		response["data"] = msg
	else:

		ipaddr = kwargs.get("ipaddr",None)
		username = kwargs.get("username",None)
		password = kwargs.get("password",None)
		command = kwargs.get("command",None)
		device_type = kwargs.get("device_type",None)
		oid = kwargs.get("oid",None)
		spath = kwargs.get("server_path",None)
		fpath = kwargs.get("router_path",None)
		
		name = ""
		snmp_data = snmp_walk(device_type=device_type,ipaddr=ipaddr,command_=oid)
		if snmp_data["success"]:
			naming_data = snmp_data["data"]
			if DEBUG:
				print "[i] snmp data : {naming_data}".format(naming_data=str(naming_data))
			name = create_name(values=naming_data,mapper=["devicename",{"extra":ipaddr},{"extra":"RUNNING_CONFIG-23-0"},"timepolled".replace("_","")])
			if name["success"]:
				name = name["data"]
				if DEBUG:
					print "[i] name : {name}".format(name=name)
			else:
				response["status"] = "Failed"
				response["data"] = "Not Reachable"
				return response
		if name:
			command = command.format(filename=name)
			result = exec_cmd(command="dir {router_path}:".format(router_path=fpath),sequences=["\n"],ipaddr=ipaddr,username=username,password=password)
			router_files = None
			if result["success"]:
				result = process_filenames_router(filenames=result["data"]["output"])
				if result["success"]:
					router_files = result["data"]
					if DEBUG:
						print "[i] ROUTER FILES : {all_files}".format(all_files=str(router_files))
		        #os.chmod("/usr/apps/oneems/config/bkup/greatlakes/", stat.S_IREAD)
			server_files = os.listdir(spath)
			if DEBUG:
				print "[i] SERVER FILES: {all_files}".format(all_files=str(server_files))

			result = file_compare(router_files=router_files,server_files=server_files)
			if result["success"]:
				result = result["data"]
				if DEBUG:
					print "[i] FILES (DELETED) : {all_files}".format(all_files=str(result))
				for filename in result.keys():
					if result[filename] == "DEL":
						delete_result = exec_cmd(command="delete {filename}".format(filename=filename),sequences=["\n","\n"],ipaddr=ipaddr,username=username,password=password)
						if delete_result["success"]:
							if DEBUG:
								print "[i] DELETED FILE : {filename}".format(filename=delete_result["data"])

			result = exec_cmd(command=command,sequences=["\n"],ipaddr=ipaddr,username=username,password=password)
			if result["success"]:
				if DEBUG:
					print "[i] SSH RESULT : {res}".format(res=str(result["data"]))
				scp_command = "scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null {username}@{ipaddr}:{fpath}:{filename} {spath}/{filename}".format(username=username,ipaddr=ipaddr,fpath=fpath,spath=spath,filename=name)
				result = get_result(command="sshpass -p {password} {scp_command}".format(scp_command=scp_command,password=password))
				if result["success"]:
					result = result["data"]
					os.chmod(spath + "/" + name,0644)
					if DEBUG:
						print "[i] SCP RESULT : {result}".format(result=str(result))
					
			else:
				if DEBUG:
					print "[x] ERROR : {err}".format(str(result["data"]))
			response["data"] = name
			response["status"] = "Success"
		else:
			if DEBUG:
				print "[x] No name assigned skipping IP: {ipaddr}".format(ipaddr=ipaddr)
	return response



def perform_cb(node_id):
	entry = fetch_from_db(tblname="nodes",filter_={"id":node_id})
	response = {"success":False, "data":None}
	if entry["status"]:
		nodes = entry["data"]
		resp = []
		for node in nodes:
			username = node.get("deviceusername",None)
			password = node.get("devicepassword",None)
			ipaddr = node.get("deviceIpAddr",None)
			if ( username and password and ipaddr):
				resp.append(config_backup(	ipaddr=ipaddr,
											username=username,
											password=password,
											device_type="2Y2LHTZP31",
											oid="1.3.6.1.2.1.1",
											command="copy system:running-config bootflash:{filename}",
											server_path="/usr/apps/oneems/config/bkup/greatlakes",
											router_path="bootflash" )	)
			else:
				if DEBUG:
					print "[x] Skipping Config backup for NODE_ID : {id_}".format(id_=node_id)
		if resp:
			if DEBUG:
				print "[i] response from scp {resp}".format(resp=str(resp))
			response["success"] = True
			response["data"] = resp
	return response

config_backup_application = connexion.App(__name__)
config_backup_application.add_api('config_backup.yaml')

if __name__ == "__main__":
	config_backup_application.run(port=8084)

