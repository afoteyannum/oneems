#!/bin/ksh

ps -ef | grep poll.py | grep -qv grep
res=$?
if [ $res == "1" ]
then
	current_dt=`date`
	print "$current_dt: Started poll" > /usr/apps/oneems/log/poll.log 2>&1
	/bin/python /usr/apps/oneems/python/poll/poll.py >> /usr/apps/oneems/log/poll.log 2>&1
fi
