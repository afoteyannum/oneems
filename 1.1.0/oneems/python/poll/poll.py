import re
import json
import datetime
import ipaddress
import connexion
import subprocess
import time
from extractor import extractor_object
from json_assembler import output_assembler
from log_ import  logger
from dynamic_mapper import CURSOR, CONN, fetch_from_db
										

mapper = {"1.3.6.1.2.1.1.5.0":extractor_object.run_all}
			


def get_result(command):
	proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
	return proc.stdout.read()

def snmp_walk(device_type, ip_addr, command_):
	response = {"result": None,"sucess":False}
	if True:
		command = "snmpwalk -v2c -t .04 -c %s %s %s"%(str(device_type),str(ip_addr),str(command_))
		result = get_result(command)
		sw_func = mapper.get(command_,None)
		response = {"status":False,"data":{}}
		if sw_func:
			response = sw_func(result)
		return response
	else:
		response["result"] = "[x] Unable to PING"
	return response



def get_ipaddrs(region):
	devices = fetch_from_db(tblname="nodes",filter_={"market": region})
	result = {"status":False,"ipaddrs":[]}
	if devices["status"]:
		devices = devices["data"]
		for device in devices:
			#ip_address = device["deviceIpAddr"]
			result["status"] = True
			result["ipaddrs"].append(device["deviceIpAddr"])
	return result

	
def poll(market="opw"):
	global schema_map
	frames_collections=[]
	xaddrs = get_ipaddrs(market)
	if xaddrs["status"]:
		xaddrs = xaddrs["ipaddrs"]
		start_time = time.time()
		for xaddr in xaddrs:
			xaddr = str(xaddr)
			snmp_frame =  snmp_walk("2Y2LHTZP31", xaddr, "1.3.6.1.2.1.1.5.0")
			frames_collection = {	xaddr:snmp_frame["data"]	}		
			frames_collections = output_assembler(frames_collections,updates=frames_collection)
			print frames_collection
		end_time = time.time()
	time_taken = str(end_time - start_time)
	print "[i] time taken for {no_dev} SNMP all devices is {time_taken} seconds in market {market}".format(no_dev=len(xaddrs),time_taken=time_taken,market=market)
	
	start_time = time.time()			
	for json_p in frames_collections:
		concatenated_frame = json_p.values()[0]
		#print concatenated_frame
		#print concatenated_frame["lastPolled"]
		xaddr = json_p.keys()[0]
		sql = "update low_priority nodes set status='{status}',lastpolled='{lastpolled}' where deviceIpAddr='{xaddr}'".format(status=concatenated_frame["status"],lastpolled = concatenated_frame["lastPolled"], xaddr=xaddr 	)
		CURSOR.execute(sql)
		CONN.commit()
	end_time = time.time()
	time_taken = str(end_time - start_time)
	print "[i] time taken for insertinng to NODES table is {time_taken}".format(time_taken=time_taken)
	
poll()
