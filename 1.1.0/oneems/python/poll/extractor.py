import re
import datetime

class Extractor:

    VER_TEMPLATE = r', Version (.*)'
    SOFTWARE_TEMPLATE = r',(.+?) Software'
    NAME_TEMPLATE = r'SNMPv2-MIB::sysName.0 = STRING: (.*)'
    UPTIME_TEMPLATE = r'() (.*) days, (.*):(.*):(.*).(.*)'
    SYSCONTACT = r'SNMPv2-MIB::sysContact.0 = STRING: (.*)'
    SYSLOCATION = r'SNMPv2-MIB::sysLocation.0 = STRING: (.*)'

    def name_conditioner(self,name,labels):
        for label in labels:
            name = name.replace(label,"")
        return name

    def index_processor(self,Q,P):
        indexes = []
        index_to_use = None
        for idx,line in enumerate(P):
            if Q in line:
                indexes.append([idx,line])

        for match in indexes:
            string = match[-1]
            index = match[0]

            test_mts = string.replace(Q,'').replace('\r\n','').strip().split('#')
            if len(test_mts)==2:
                if test_mts[-1] == '':
                    index_to_use =  index
                    break
        return index_to_use

    def segment_extract(self,SEGMENT,string_to_search):
        # Fetch only section lines
        regex = re.compile(SEGMENT)
        line_data = []
        found = False
        first_occurence = True
        for line in string_to_search:
            if regex.search(line) or found:
                found = True
                if not first_occurence:
                    if(line.find("#") != -1 ):
                        break
                    else:
                        line_x = line.strip()
                        line_x = " ".join(line_x.split())
                        line_data.append(line_x)
                first_occurence = False
        return line_data

    def extract_version(self, string_to_search):
        m = re.search(Extractor.VER_TEMPLATE, string_to_search)
        if m:
            return m.group(1).split(',')[0].strip()
        return " "

    def extract_software(self, string_to_search):
        m = re.search(Extractor.SOFTWARE_TEMPLATE, string_to_search)
        if m:
            return m.group(1).strip()
        return " "

    def extract_name(self, string_to_search):
        m = re.search(Extractor.NAME_TEMPLATE, string_to_search)
        if m:
            return m.group(1).strip()
        return " "

    def extract_uptime(self, string_to_search):
        m = re.search(Extractor.UPTIME_TEMPLATE, string_to_search)
        if m:
            return m.group(2).split(')')[1].strip() + ' days ' + m.group(3)+':'+m.group(4)
        return " "

    def extract_syscontact(self, string_to_search):
        m = re.search(Extractor.SYSCONTACT, string_to_search)
        if m:
            return m.group(1).split(',')[0].strip()
        return " "

    def extract_syslocation(self, string_to_search):
        m = re.search(Extractor.SYSLOCATION, string_to_search)
        if m:
            return m.group(1).split(',')[0].strip()
        return " "


    def run_all(self,string_to_search=open('dummy.txt').read()):
        ver = self.extract_version(string_to_search)
        software = self.extract_software(string_to_search)
        name = self.extract_name(string_to_search)
        uptime = self.extract_uptime(string_to_search)
        syscontact = self.extract_syscontact(string_to_search)
        syslocation = self.extract_syslocation(string_to_search)
        if name.find(".") == -1:
    		return {"data": {  "lastPolled":datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                "status": "0"},
                    "status": False}
    	else:
    		return {"data":{"lastPolled":datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                            "status": "1"},
                    "status": True}


extractor_object = Extractor()
